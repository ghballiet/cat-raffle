import { Button, Display, Divider, Modal, Page, Text, useModal } from "@geist-ui/core";
import JSConfetti from "js-confetti";
import { useState } from "react";

function names(txt: string, num: number): string[] {
  const out: string[] = [];
  const lines = txt.split("\n");
  const seen: {[k: string]: Boolean} = {};
  let i=0;
  while(i < num) {
    const idx = Math.floor(Math.random() * (lines.length - 1));
    const parts = lines[idx].split(",");
    const name = `${parts[1]} ${parts[2]}`;
    if (!!seen[name]) {
      continue;
    }

    seen[name] = true;
    out.push(name);
    i++;
  }

  return out;
}

function App() {
  const jsc = new JSConfetti();
  const { setVisible, bindings } = useModal();
  const [tier1, setTier1] = useState<JSX.Element[]>([]);
  const [tier2, setTier2] = useState<JSX.Element>();

  const init = async () => {
    const t1r = await fetch('/data/tier1.csv');
    const t1s = await t1r.text();
    const t1w = names(t1s, 4);
    const t1e = t1w.map(n => {
      return (<Text key={n}>{n}</Text>)
    });
    setTier1(t1e);

    const t2r = await fetch('/data/tier2.csv');
    const t2s = await t2r.text();
    const t2w = names(t2s, 1);
    const t2e = (<Text>{t2w[0]}</Text>);
    setTier2(t2e);
  }

  const unwrap = () => {
    init();
    jsc.addConfetti();
    setVisible(true);
  }

  return (
    <>
      <Page>
        <Display>
          <Button placeholder='Button' scale={10} onClick={unwrap}>&#x1f381;</Button>
        </Display>
        <Modal {...bindings}>
          <Modal.Title>Woo-hoo! &#127881;</Modal.Title>
          <Modal.Content>
            <Text h3>Tier 1 Winners</Text> 
            {tier1}
            <Divider />
            <Text h3>Tier 2 Winner</Text>
            {tier2}
          </Modal.Content>
        </Modal>
      </Page>
    </>
  )
}

export default App
